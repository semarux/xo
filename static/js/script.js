

var game=false;
var player=false;

function update_field(field){
    if (field != undefined){
        for (var i=0; i<field.length; i++){
            for (var j=0; j<field[i].length; j++){
                $(".row_"+i + " .cell_" + j).removeClass("x");
                $(".row_"+i + " .cell_" + j).removeClass("o");
                $(".row_"+i + " .cell_" + j).text("");
                if (field[i][j] != "-"){
                    $(".row_"+i + " .cell_" + j).text(field[i][j]).addClass(field[i][j]);
                }
            }
        }
    }
    return false;
}

function inform(mess){
    $(".informer").text(mess);
    $(".informer").fadeIn(600,function(){
        $(".informer").fadeOut(1500);
    });
    return false
}

var updater = {
    socket: null,
    start: function() {
        var url = "ws://" + location.host + "/ws";
        updater.socket = new WebSocket(url);
        updater.socket.onmessage = function(event) {
            data = JSON.parse(event.data);
            if (data["status"] == "start"){
                game = data["game"];
                player = data["player"];
            }else if (data["status"]=="complete"){
                game = false;
                $("#start").show();
            }
            update_field(data["field"]);
            if (data["message"]!=""){
                inform(data["message"]);
            }


        }
    },

};


$(document).ready(function(){
    function update_game(){
        if (game!=false) {
            updater.socket.send(JSON.stringify({"game": game, "action": "update", "player": player}));
        }
        return false;
    }
    setInterval(update_game, 500);
    updater.start();
    $("#start").on("click", function(){
        updater.start();
        $(this).hide()
    });

    $(".cell").hover(function() {
        $(this).addClass("hover");
    }, function() {
            $(this).removeClass("hover");
    });

    $(".cell").on("click", function(){
//        var i = parseInt($(this).closest(".row").attr("id").split("_")[1]);
//        var j = parseInt($(this).attr("id").split("_")[1]);
        var i = parseInt($(this).closest(".row").attr("data"));
        var j = parseInt($(this).attr("data"));
        updater.socket.send(JSON.stringify({"game": game, "action": "step", "player": player, "i": i, "j": j}));
    });


});

