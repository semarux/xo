#-*- coding: utf-8 -*-
# player_x = raw_input("Enter your name:")
# player_o = raw_input("Enter your name:")

player_act = "x"
# print "First step is player %s" % player_x


def new_field():
    return [
        ["-", "-", "-"],
        ["-", "-", "-"],
        ["-", "-", "-"],
    ]

field = new_field()

def check_step(x, y):
    if x > 2 or x < 0:
        return False
    elif y > 2 or y < 0:
        return False
    elif field[x][y] == "-":
        return True
    return False


def require_field(x, y, player):
    field[x][y] = player


def printfield(field):
    for row in field:
        for cell in row:
            print cell,
        print ""


def check_is_win(player_act):
    diag1 = 0
    diag2 = 0
    for i, row in enumerate(field):
        hor = 0
        ver = 0
        for j, cell in enumerate(row):
            if field[i][j] == player_act:
                hor += 1
            if field[j][i] == player_act:
                ver += 1

        if hor == 3 or ver == 3:
            return True

        if field[i][i] == player_act:
            diag1 += 1
        if field[i][2-i] == player_act:
            diag2 += 1

    if diag1 == 3 or diag2 == 3:
        return True
    return False

while 1:
    print "---------"
    print "player %s" % player_act
    step_x = raw_input("x:")
    step_y = raw_input("y:")

    if step_x == "quit" or step_y == "quit":
        exit()
    try:
        step_x, step_y = int(step_x), int(step_y)
    except:
        print "Enter a valid coordinates"
    else:
        if check_step(step_x, step_y):
            require_field(step_x, step_y, player=player_act)
            printfield(field)
            if check_is_win(player_act):
                print "Player %s win!!!" % player_act
                print "New game"
                field = new_field()
            else:
                if player_act == "x":
                    player_act = "0"
                elif player_act == "0":
                    player_act = "x"
        else:
            print "You don't check this place"











