import tornado.ioloop
import tornado.web
import tornado.escape
import tornado.websocket
import tornado.wsgi
import os.path
from tornado.options import define, options, parse_command_line
import wsgiref.simple_server

define("port", default=80, type=int)
from uuid import uuid4

games = []

clients = []


def new_field():
    return [
    ["-", "-", "-"],
    ["-", "-", "-"],
    ["-", "-", "-"],
]


def check_step(x, y, field) :
    if x > 2 or x < 0:
        return False
    elif y > 2 or y < 0:
        return False
    elif field[x][y] == "-":
        return True
    return False


def check_is_win(player_act, field):
    diag1 = 0
    diag2 = 0
    for i, row in enumerate(field):
        hor = 0
        ver = 0
        for j, cell in enumerate(row):
            if field[i][j] == player_act:
                hor += 1
            if field[j][i] == player_act:
                ver += 1

        if hor == 3 or ver == 3:
            return True

        if field[i][i] == player_act:
            diag1 += 1
        if field[i][2-i] == player_act:
            diag2 += 1

    if diag1 == 3 or diag2 == 3:
        return True
    return False


def get_incomplete_game():
    for game in games:
        if len(game["players"]) == 1:
            return game
    return False


def get_game(game_id):
    for game in games:
        if game["id"] == game_id:
            return game
    return game


def printfield(field):
    for row in field:
        for cell in row:
            print cell,
        print ""


def require_field(game, player, i, j):
    game["field"][i][j] = player


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    def open(self, *args):
        print "New connection"
        clients.append(self)
        game = get_incomplete_game()
        player = "%s" % uuid4()
        if game:
            game["players"].update({player: "o"})
            self.write_message({"game": game["id"], "player": player,  "message": "Welcome! Yor connect to game", "status": "start"})
        else:
            game = {
                "id": "%s" % uuid4(),
                "field": new_field(),
                "players": {player: "x"},
            }
            games.append(game)
            self.write_message({"game": game["id"], "player": player, "message": "Welcome! Yor create game", "status": "start"})

    def on_message(self, message):
        data = tornado.escape.json_decode(message)
        game = get_game(data.get("game"))
        player = game["players"].get(data.get("player"))
        if game.get("status") == "complete":
                game["message"] = "Player %s win!!!" % game["winner"]
        else:
            if data.get("action") == "step":
                # print "%s=%s" % (game.get("player_turn"), player)
                # print "-----"
                if game.get("player_turn", "") != "" and game.get("player_turn", "") != player:
                    game["status"] = "error"
                    game["message"] = "Is not your turn"
                else:
                    try:
                        i, j = int(data.get("i")), int(data.get("j"))
                    except:
                        game["status"] = "error"
                        game["message"] = "Enter a valid coordinates"
                    else:
                        if check_step(i, j, game["field"]):
                            game["status"] = "ok"
                            require_field(game, player, i, j)
                            if check_is_win(player, game["field"]):
                                game["status"] = "complete"
                                game["winner"] = player
                                game["message"] = "Player %s win!!!" % player
                            else:
                                if player == "x":
                                    game["player_turn"] = "o"
                                elif player == "o":
                                    game["player_turn"] = "x"
                        else:
                            game["status"] = "error"
                            game["message"] = "You don't check this place"
            elif data.get("action") == "update":
                    game["status"] = "updated"
                    game["message"] = ""
            else:
                game["status"] = "undefined"
                game["message"] = ""
        # print "-----------------"
        # printfield(game["field"])
        self.write_message(game)


    def on_close(self):
        print "Connection closed"

app = tornado.web.Application(
    [
        (r'/', IndexHandler),
        (r'/ws', WebSocketHandler),
    ],
    template_path=os.path.join(os.path.dirname(__file__), "templates"),
    static_path=os.path.join(os.path.dirname(__file__), "static"),
)


if __name__ == '__main__':
    wsgi_app = tornado.wsgi.WSGIAdapter(app)
    server = wsgiref.simple_server.make_server('', 80, wsgi_app)
    server.serve_forever()
    # app.listen(options.port)
    # tornado.ioloop.IOLoop.instance().start()